@Grab('mysql:mysql-connector-java:5.1.36')
@GrabConfig(systemClassLoader=true)
import groovy.sql.Sql
import groovy.swing.impl.DefaultAction
import groovyx.gpars.GParsExecutorsPool
import org.apache.commons.cli.Option
@Grab('org.yaml:snakeyaml:1.19')
import org.yaml.snakeyaml.Yaml

import java.sql.Types

//Algunas clases

//Algunas funciones

def executeOnShell(String command, File workingDir) {
    def output = []
    Process process = new ProcessBuilder(addShellPrefix(command))
            .directory(workingDir)
            .redirectErrorStream(true)
            .start()
    process.inputStream.eachLine {output += it}
    process.waitFor()
    return output
}

private def addShellPrefix(String command) {
    def commandArray = new String[3]
    commandArray[0] = "sh"
    commandArray[1] = "-c"
    commandArray[2] = command
    return commandArray
}

def genRandomString = {
    def alphabet = (('A'..'Z')+('0'..'9')).join()
    new Random().with {
        (1..9).collect { alphabet[ nextInt( alphabet.length() ) ] }.join()
    }
}


//Arranca aqui
Map config = new Yaml().load(new File('config.yml').text)

def cli = new CliBuilder(usage: 'importer.groovy -[t] [table] [index] [id]')

cli.with {
    h 'Show usage information'
    tabla args: 1, 'Nombre de la tabla SQL a importar'
    indice args: 1, 'Index destino en Elastic'
    id args: 1, 'Columna ID de la tabla SQL'
    file args: 1, 'Nombre de file donde buscar listado de parametros'
//    fechas args: Option.UNLIMITED_VALUES, valueSeparator: ',' as char, 'Campos fecha para validar el pattern 0000-00-00, separados por coma'
}

def options = cli.parse(args)
if (!options) {
    cli.usage()
    return
}

if (options.h) {
    cli.usage()
    return
}

def parsearOptions = {args ->
    def parsedOptions = cli.parse(args)
    def opciones = [tabla: parsedOptions.tabla,
            indice: parsedOptions.indice,
            id: parsedOptions.id
    ]
    opciones
}

def inputs = []

if (options.file) {
    File f = new File(options.file)
    f.eachLine {
        if (!it.trim().startsWith('#')){
            inputs << parsearOptions(it.trim())
        }
    }
} else {
    inputs << parsearOptions(args)
}

println inputs

def table, index, id, offsetInicial
Sql sql = Sql.newInstance(config.url, config.username, config.password, config.driverClassName)

//Busco el Driver JDBC desde el repo de Grape
def _ = File.separator
String dirJar = "${System.getProperty('user.home')}${_}.groovy${_}grapes${_}mysql${_}mysql-connector-java${_}jars${_}"
String jar = new File(dirJar).list().first()
String pathJar = dirJar + jar

def masterTextos = []
inputs.each {input ->

    if (!input) {
        return
    }

    table = input.tabla
    index = input.indice
    id = input.id
    offsetInicial = input.size > 3 ? input[3] as Integer : null

    String queryMetadata = 'select * from ' + table + ' limit 1'
    def md
    sql.eachRow(queryMetadata) {row ->
        md = row.getMetaData()
    }

    def selectQuery = 'select '
    md.columnCount.times {i ->
        def colname = md.getColumnLabel(i + 1)
        if (md.getColumnType(i + 1) in [Types.TIME, Types.TIMESTAMP, Types.DATE ]) {
            selectQuery += "case when $colname = '0000-00-00' then null else $colname end as $colname,"
        } else {
            selectQuery += "$colname,"
        }
    }
    selectQuery = selectQuery.substring(0, selectQuery.length() - 1)
    selectQuery += ' from ' + table

    def getConfText = { psize, poffset->
        """input {
            jdbc {
                jdbc_connection_string => "$config.url"
                jdbc_user => "$config.username"
                jdbc_password => "$config.password"
                jdbc_driver_library => "$pathJar"
                jdbc_driver_class => "$config.driverClassName"
                statement => "$selectQuery order by $id limit $psize ${poffset > 0 ? 'offset ' + poffset : ''}"
            }
        }
        output {
            elasticsearch {
                "hosts" => "localhost:9200"
                "index" => "$index"
                "document_id" => "%{$id}"
            }
        }
        """
    }

    try {
        String queryCount = 'select count(*) from ' + table
        println 'Ejecutando query: ' + queryCount
        Integer count = sql.firstRow(queryCount).values().first() as Integer
        Integer size = config.jdbcSize as Integer
        Integer offset = offsetInicial ?: 0
        count -= offset

        println "Count: $count"

        def textos = []
        while (count > 0) {
            textos << getConfText(size, offset)
            count -= size
            offset += size
        }

        if (textos) {
            masterTextos << textos
        }
    } catch (Exception e) {
        e.printStackTrace()
        return
    }
}

int cantThreads = config.actors ?: 1
cantThreads = Math.ceil(masterTextos.size() / cantThreads) as Integer

GParsExecutorsPool.withPool {
    def ejecutarTextos = { textos ->
        File file = new File(genRandomString() + '.conf')
        File pathData = new File(genRandomString())
        pathData.mkdir()
        String comando = "/usr/share/logstash/bin/logstash --path.settings=$config.logstashSettingsPath --path.data=$pathData.name -f $file.name"
        println "Ejecutando: $comando"
        textos.each { texto ->
            file.write texto
            println texto.readLines().find { it.trim().startsWith('statement') }.trim()
            executeOnShell(comando, new File('.')).each {
                println it
            }
        }
        file.delete()
        pathData.deleteDir()
    }

    masterTextos.collate(cantThreads).each {
        ejecutarTextos.callAsync(it.flatten())
    }

}

println 'Terminado'
